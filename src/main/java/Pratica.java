import java.lang.Math;
import utfpr.ct.dainf.pratica.PontoXZ;
import utfpr.ct.dainf.pratica.PontoXY;

public class Pratica
{
  public static void main(String[] args)
  {
    PontoXZ xz = new PontoXZ(-3, 2);
    PontoXY xy = new PontoXY(0, 2);
    
    System.out.println(String.format("Distancia: %f", xz.dist(xy)));
  }
}