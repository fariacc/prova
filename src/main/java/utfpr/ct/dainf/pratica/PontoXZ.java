package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861735
 */
public class PontoXZ extends Ponto2D {
    public PontoXZ (double _x, double _z) {
	setX(_x);
      	setY(0);
      	setZ(_z);
    }
  
  @Override
    public String toString() {
      return String.format("%s(%f,%f)", getNome(), getX(), getZ());
    }
}
