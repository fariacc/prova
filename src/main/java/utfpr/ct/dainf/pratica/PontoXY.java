package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861735
 */
public class PontoXY extends Ponto2D {
    public PontoXY (double _x, double _y) {
        setX(_x);
      	setY(_y);
      	setZ(0);
    }
  
  @Override
    public String toString() {
      return String.format("%s(%f,%f)", getNome(), getX(), getY());
    }
}