package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861735
 */
public class PontoYZ extends Ponto2D {
    public PontoYZ (double _y, double _z) {
	setX(0);
      	setY(_y);
      	setZ(_z);
    }

    @Override
    public String toString() {
      return String.format("%s(%f,%f)", getNome(), getY(), getZ());
    }
}