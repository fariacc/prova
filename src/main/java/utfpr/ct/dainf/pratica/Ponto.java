package utfpr.ct.dainf.pratica;

public class Ponto {
    private double x, y, z;

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
  
  public Ponto() {
      
    x = 0;
    y = 0;
    z = 0;
  }
  
  public Ponto(double _x, double _y, double _z) {
    x = _x;
    y = _y;
    z = _z;
  }
  
  public double getX () {
    return x;
  }
  public void setX (double _x) {
    x = _x;
  }
  
  public double getY () {
    return y;
  }
  public void setY (double _y) {
    y = _y;
  }
  
  public double getZ () {
    return z;
  }
  public void setZ (double _z) {
    z = _z;
  }
  
  @Override
  public String toString() {
    return String.format("%s(%f,%f,%f)", getNome(), getX(), getY(), getZ());
  }
  
  @Override
  public boolean equals(Object _o) {
    Ponto _p = (Ponto)_o;
    return _p.x == x && _p.y == y && _p.z == z;
  }
  
  public double dist (Ponto _p) {
    return Math.sqrt(
    	Math.pow(_p.x - x, 2) + 
        Math.pow(_p.y - y, 2) + 
        Math.pow(_p.z - z, 2)
    );
  }
}